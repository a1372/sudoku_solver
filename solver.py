
from sudoku_table import SudokuTable


class Solver:
    """
    Solves sudoku based on SudokuTable
    """
    def __init__(self, sudoku_table: SudokuTable):
        """
        :param sudoku_table: SudokuTable instance
        """
        self.sudoku_table = sudoku_table

    def solve(self, current_cell=0):
        """
        Solves sudoku recursively
        :param current_cell: cell to set value for
        :return: True or False :)
        """
        cell = self.sudoku_table.list_cells[current_cell]
        cell_row = cell[0]
        cell_col = cell[1]
        next_cell = current_cell + 1
        for value in range(1, 10):
            self.sudoku_table.set_cell_value(row=cell_row, col=cell_col, value=value)
            if not(self.sudoku_table.duplicates_in_row(row_num=cell_row) or
                   self.sudoku_table.duplicates_in_col(col_num=cell_col) or
                   self.sudoku_table.duplicates_in_section(row_num=cell_row, col_num=cell_col)):
                if self.sudoku_table.list_cells[-1] == cell:
                    return True
                elif self.solve(current_cell=next_cell):
                    return True
            else:
                pass
        self.sudoku_table.set_cell_value(row=cell_row, col=cell_col, value=0)
        return False


if __name__ == "__main__":
    import time
    from statics import testing_table

    START_TIME = time.time()

    my_table = SudokuTable(list2d=testing_table)
    solver = Solver(sudoku_table=my_table)
    my_table.print_table()
    solver.solve()
    my_table.print_table()
    print(round(time.time() - START_TIME, 2), "seconds")
