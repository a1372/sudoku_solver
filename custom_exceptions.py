

class IncorrectDims(Exception):
    """
    Exception if provided table incorrect
    """
    def __init__(self, message='Incorrect table dimensions'):
        super().__init__(message)


class IncorrectType(Exception):
    def __init__(self, message='Incorrect value type in table'):
        super().__init__(message)


class IncorrectCellValue(Exception):
    """
    Exception if provided table have any incorrect values according to sudoku rules
    """
    def __init__(self, message='Incorrect provided table values'):
        super().__init__(message)


class DuplicateFound(Exception):
    def __init__(self, message='Duplicate found'):
        super().__init__(message)
