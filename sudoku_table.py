from statics import testing_table
from custom_exceptions import IncorrectDims, IncorrectCellValue, DuplicateFound, IncorrectType
from pprint import pprint


class SudokuTable:
    """
    Class represents sudoku table
    """
    def __init__(self, list2d):
        self.__table = list2d
        self.check_table()
        self.list_cells = self.cells_to_solve()

    def check_dims(self):
        """
        Checks dimensions of provided table
        :raises IncorrectDims: if table not 9x9
        :return: True
        """
        if len(self.__table) == 9:
            for row in self.__table:
                if len(row) != 9:
                    raise IncorrectDims
        else:
            raise IncorrectDims
        return True

    def check_values(self):
        """
        Checks if table data are correct
        :raises IncorrectType: if cell type not int
        :raises IncorrectCellValue: if cell value not in [0..9]
        :raises DuplicateFound: if duplicate in section, row or column
        :return: True
        """
        for row in self.__table:
            for cell in row:
                if type(cell) is not int:
                    raise IncorrectType
                elif -1 < cell < 10:
                    pass
                else:
                    raise IncorrectCellValue

        for i in range(9):
            if self.duplicates_in_col(col_num=i) or self.duplicates_in_row(row_num=i):
                raise DuplicateFound

            if not i // 3:
                for r in range(0, 9, 3):
                    if self.duplicates_in_section(row_num=r, col_num=i):
                        raise DuplicateFound

        return True

    def check_table(self):
        """
        General method to check initial table
        :return: True
        """
        self.check_dims()
        self.check_values()
        return True

    def cells_to_solve(self):
        """
        Builds list of cell indexes that needs to be filled
        :return: list of cell indexes
        """
        cells = []
        for i in range(0, 9):
            for j in range(0, 9):
                if self.__table[i][j] == 0:
                    cells.append([i, j])
        return cells

    def filter_values(self, list_):
        """
        Filters values in provided list
        :param list_: list of numbers
        :return: filtered list
        """
        return list(filter(lambda i: i != 0, list_))

    def duplicates_in_list(self, list_):
        """
        Checks if there is duplicates
        :param list_: list to check
        :return: True if there is a duplicate else False
        """
        return any(list_.count(elem) > 1 for elem in list_)

    def duplicates_in_row(self, row_num):
        """
        Checks if there is duplicates in row
        :param row_num: number of row
        :return: True if there is a duplicate else False
        """
        row_to_check = self.filter_values(self.__table[row_num])
        return self.duplicates_in_list(row_to_check)

    def duplicates_in_col(self, col_num):
        """
        Checks if there is duplicates in col
        :param col_num: number of col
        :return: True if there is a duplicate else False
        """
        col = []
        for row in self.__table:
            col.append(row[col_num])
        col_to_check = self.filter_values(col)
        return self.duplicates_in_list(col_to_check)

    def duplicates_in_section(self, row_num, col_num):
        """
        Checks if there is duplicates in section
        :param row_num: row number of a cell
        :param col_num: col number of a cell
        :return: True if there is a duplicate else False
        """
        square_start_x = (col_num // 3) * 3
        square_start_y = (row_num // 3) * 3
        square_end_x = square_start_x + 3
        square_end_y = square_start_y + 3
        buffer_list = []
        for i in range(square_start_y, square_end_y):
            for j in range(square_start_x, square_end_x):
                buffer_list.append(self.__table[i][j])
        list_square_elems = self.filter_values(buffer_list)
        return self.duplicates_in_list(list_square_elems)

    def set_cell_value(self, row, col, value):
        """
        Sets cell value
        :param row: row number of a cell
        :param col: col number of a cell
        :param value: new cell value
        :return: None
        """
        self.__table[row][col] = value

    def get_cell_value(self, row, col):
        """
        Gets cell value
        :param row: row number of a cell
        :param col: col number of a cell
        :return: cell value
        """
        return self.__table[row][col]

    def print_table(self):
        """
        Prints table
        :return: None
        """
        pprint(self.__table)


if __name__ == "__main__":
    my_table = SudokuTable(list2d=testing_table)
